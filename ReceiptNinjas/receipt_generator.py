# Tutorial https://github.com/NielsRogge/Transformers-Tutorials/blob/master/Donut/CORD/Fine_tune_Donut_on_a_custom_dataset_(CORD)_with_PyTorch_Lightning.ipynb
from datasets import load_dataset
from ast import literal_eval

dataset = load_dataset("naver-clova-ix/cord-v2")


example = dataset['train'][0]
image = example['image']
# let's make the image a bit smaller when visualizing
ground_truth = example['ground_truth']

print(literal_eval(ground_truth)['gt_parse'])
